# Stage Renku

Project to easily deploy a staging environment of Renku.

## Usage

Fill in:
- `charts.yaml`

To update the deployed values, got to [stage-renku-deploy](https://gitlab.com/renku/stage-renku-deploy)
and edit `values.yaml`.

Once you commit, find the newly-created CI-job on the `CI/CD --> Jobs` page
and click on the play button.

## Enabling CI on the staging deployment

If you create a fresh deployment, there is probably no CI runner registered.
You can register one by logging in to the switch VM used specifically for
this purpose:

```
$ ssh -i ~/.ssh/sdsc-key \
    centos@192.168.10.4 \
    -o ProxyCommand="ssh -i ~/.ssh/sdsc-key -W %h:%p -q centos@86.119.30.76"
```

where the `sdsc-key` is the sdsc private key. Once logged in, you can register
the runner with:

```
sudo docker exec -ti gitlab-runner gitlab-runner register \
    -n -u https://staging.testing.datascience.ch/gitlab/ \
    --name docker-switch \
    -r 70578e7816b04865aec5d92a74aaee05 \
    --executor docker \
    --locked=false \
    --run-untagged=true \
    --docker-image="docker:stable" \
    --docker-privileged \
    --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
    --tag-list image-build \
    --docker-pull-policy "if-not-present"
```

## How it works

The scripts are similar to <https://github.com/SwissDataScienceCenter/renku/blob/master/scripts/minikube_deploy.py>,
they assemble the helm chart by hand with the help of chartpress.
