# -*- coding: utf-8 -*-

"""Builds and updates the renku-ui chart
"""


import os
from subprocess import run

from ruamel.yaml import YAML


def main():
    yaml = YAML(typ='rt')
    yaml.indent(mapping=2, offset=2, sequence=4)

    charts_file = os.path.join(os.path.dirname(__file__), '..', 'charts.yaml')
    with open(charts_file, 'rt') as f:
        charts = yaml.load(f)

    image_prefix = charts.get('imagePrefix')

    dependencies = charts.get('dependencies', [])
    for dep in dependencies:
        name = dep.get('name')
        repo = dep.get('repo')
        refspec = dep.get('refspec')

        repo_dir = '/tmp/' + name

        run(['rm', '-rf', repo_dir]).check_returncode()
        run(['git', 'clone', repo, repo_dir]).check_returncode()
        run(['git', 'checkout', refspec], cwd=repo_dir).check_returncode()
        chart_dir = os.path.join(repo_dir, 'helm-chart')
        run(['chartpress', '--image-prefix', image_prefix, '--push'], cwd=chart_dir).check_returncode()


if __name__ == '__main__':
    main()
