# -*- coding: utf-8 -*-

"""Updates the renku chart with the desired subcharts
"""


import os
from subprocess import run

from ruamel.yaml import YAML


def main():
    yaml = YAML(typ='rt')
    yaml.indent(mapping=2, offset=2, sequence=4)

    charts_file = os.path.join(os.path.dirname(__file__), '..', 'charts.yaml')
    with open(charts_file, 'rt') as f:
        charts = yaml.load(f)

    renku_repo_dir = '/tmp/renku'

    renku_chart_dir = os.path.join(renku_repo_dir, 'charts', 'renku')
    with open(os.path.join(renku_chart_dir, 'requirements.yaml'), 'rt') as f:
        renku_requirements = yaml.load(f)

    dependencies = charts.get('dependencies', [])
    for dep in dependencies:
        name = dep.get('name')
        repo_dir = '/tmp/' + name

        chart_dir = os.path.join(repo_dir, 'helm-chart', name)
        with open(os.path.join(chart_dir, 'Chart.yaml'), 'rt') as f:
            chart = yaml.load(f)
        version = chart.get('version')

        req = next(filter(lambda x: x.get('name') == name, renku_requirements.get('dependencies')))
        req['version'] = version
        req['repository'] = 'file://' + chart_dir
        run(['helm', 'dep', 'update', name], cwd=os.path.join(repo_dir, 'helm-chart')).check_returncode()

    with open(os.path.join(renku_chart_dir, 'requirements.yaml'), 'wt') as f:
        yaml.dump(renku_requirements, f)

    run(['helm', 'dep', 'update', 'renku'], cwd=os.path.join(renku_repo_dir, 'charts')).check_returncode()
    output_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'output'))
    run(['mkdir', '-p', output_dir]).check_returncode()
    run(['helm', 'package', 'renku', '-d', output_dir], cwd=os.path.join(renku_repo_dir, 'charts')).check_returncode()


if __name__ == '__main__':
    main()
