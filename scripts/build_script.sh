#!/usr/bin/env bash

set -ex

helm repo add renku https://swissdatasciencecenter.github.io/helm-charts
helm repo add gitlab https://charts.gitlab.io
helm repo add jupyterhub https://jupyterhub.github.io/helm-chart
helm repo update

python scripts/checkout_dependencies.py
python scripts/checkout_renku.py
python scripts/update_renku.py
